Расширение Themes для Yii2
=============================

Данное расширение позволяет изменять темы бутсрап для проекта.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Add to composer.json:
```json
"emilasp/yii2-bootstrap-themes": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-bootstrap-themes.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

В лейаут добавляем виджет расширения и указываем ему какую темы использовать.
```php

<?php echo \emilasp\core\extensions\themes\Themes::widget([ 'theme'=>Yii::$app->params['theme'] ]); ?>

```
Параметр theme = \emilasp\core\extensions\themes\Themes::THEME_..

