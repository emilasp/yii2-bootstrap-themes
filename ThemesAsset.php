<?php

namespace emilasp\themes;
use kartik\widgets\AssetBundle;

class ThemesAsset extends AssetBundle
{

    public $jsOptions=['position'=>1];

    public static  $theme = '';

    public static function setTheme( $view, $theme ){
        self::$theme = $theme;
        self::register($view);
    }

    //public $sourcePath = 'emilasp/account/extensions/AdminSkin/assets/';

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public function init()
    {
        $themesPath = 'themes/';
        $this->setSourcePath(__DIR__ . '/assets');
        //$this->setupAssets('js', ['alert','admin']);
        $theme = $themesPath.self::$theme;
        $this->setupAssets('css', [$theme]);
        parent::init();
    }


}
